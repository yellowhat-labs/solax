package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/yellowhat-labs/solax-exporter/internal/data"
	"gitlab.com/yellowhat-labs/solax-exporter/internal/metrics"
)

func main() {
	registry, metrics := metrics.NewMetrics()

	url := os.Getenv("SOLAX_URL")
	pwd := os.Getenv("SOLAX_PASSWORD")

	log.Printf("Scraping data from %s\n", url)

	mux := http.NewServeMux()
	httpServer := &http.Server{
		Addr:              "0.0.0.0:9090",
		Handler:           mux,
		ReadHeaderTimeout: 5 * time.Second,
	}

	promhttpHandlerOpts := promhttp.HandlerOpts{
		Registry: registry,
		// https://pkg.go.dev/github.com/prometheus/client_golang/prometheus/promhttp#HandlerOpts
		ErrorLog:            nil,
		ErrorHandling:       promhttp.PanicOnError,
		DisableCompression:  false,
		OfferedCompressions: nil,
		MaxRequestsInFlight: 1,
		Timeout:             5 * time.Second,
		EnableOpenMetrics:   true,
		// Disabled as it increase cardinality significantly
		EnableOpenMetricsTextCreatedSamples: false,
		ProcessStartTime:                    time.Now(),
	}
	promHandler := promhttp.InstrumentMetricHandler(
		registry,
		promhttp.HandlerFor(registry, promhttpHandlerOpts),
	)

	mux.HandleFunc("/metrics", func(writer http.ResponseWriter, request *http.Request) {
		dataParsed, err := data.Get(url, pwd)
		if err != nil {
			// During the night the inverter shuts down therefore `data.Get` will timeout
			// log.Printf("%v", err)
			// log.Fatal(err)
			return
		}

		metrics.Update(dataParsed)

		promHandler.ServeHTTP(writer, request)
	})

	log.Printf("Starting HTTP server, listening on %s\n", httpServer.Addr)

	err := httpServer.ListenAndServe()
	if err != nil {
		log.Fatal("Failed to start serving HTTP requests: ", err)
	}
}
