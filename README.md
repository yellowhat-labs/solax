# SolaX

Prometheus exporter for SolaX Power X1-3.0-S-D(L)

## Links

* <https://community.openenergymonitor.org/t/reading-data-from-a-solax-x1-hybrid-gen4-hybridx-inverters/23842>
* <https://www.photovoltaikforum.com/thread/145251-auswertung-von-solax-x3-hybrid-inverter/>
* <https://www.photovoltaikforum.com/thread/209638-solax-x1-pocket-lan-vs-wifi/>
* <https://github.com/wills106/homeassistant-config/blob/master/experimental/solax_test.yaml>
* <https://github.com/GideonPARANOID/solax-data-acquisition>
* <https://github.com/Marvo2011/solar-meter-modbus>
* <https://github.com/think-free/solax>
* <https://github.com/squishykid/solax>
* <https://github.com/RubenGamezTorrijos/SolaxPower-SK-SU5000E>
