# Mock solax-exporter for air-gapped use

1. Start:

    ```bash
    make mock-up
    ```

2. Test `prism`:

    ```bash
    curl -X POST localhost:4010 --data 'optType=ReadRealTimeData&pwd=SerialNumber'
    ```

3. Test `solax-exporter`:

    ```bash
    curl localhost:9090/metrics
    ```

4. Stop:

    ```bash
    make mock-down
    ```
