package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"gitlab.com/yellowhat-labs/solax-exporter/internal/data"
)

type Metrics struct {
	infoSN               *prometheus.GaugeVec
	infoVer              *prometheus.GaugeVec
	scrapeSeconds        prometheus.Gauge
	temp                 prometheus.Gauge
	acVolt               prometheus.Gauge
	acAmpere             prometheus.Gauge
	acWatt               prometheus.Gauge
	acFreq               prometheus.Gauge
	pv1Volt              prometheus.Gauge
	pv1Ampere            prometheus.Gauge
	pv1Watt              prometheus.Gauge
	pv2Volt              prometheus.Gauge
	pv2Ampere            prometheus.Gauge
	pv2Watt              prometheus.Gauge
	energyGeneratedTotal prometheus.Gauge
	energyGeneratedToday prometheus.Gauge
	energyExportedTotal  prometheus.Gauge
	energyImportedTotal  prometheus.Gauge
	powerExported        prometheus.Gauge
}

func NewMetrics() (*prometheus.Registry, *Metrics) {
	registry := prometheus.NewRegistry()

	metrics := &Metrics{
		infoSN: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "info",
				Name:      "sn",
				Help:      "Solax Serial Number",
			},
			[]string{"SN"},
		),
		infoVer: prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "info",
				Name:      "ver",
				Help:      "Solax firmware version",
			},
			[]string{"Ver"},
		),
		scrapeSeconds: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "scrape",
				Name:      "seconds",
				Help:      "Time to scrape from SOLAX",
			},
		),
		temp: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name:      "celsius",
				Subsystem: "",
				Namespace: "solax",
				Help:      "Inverter temperature",
			},
		),
		acVolt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "ac",
				Name:      "volt",
				Help:      "AC Voltage",
			},
		),
		acAmpere: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "ac",
				Name:      "ampere",
				Help:      "AC Current",
			},
		),
		acWatt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "ac",
				Name:      "watt",
				Help:      "AC Power",
			},
		),
		acFreq: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "ac",
				Name:      "freq_hz",
				Help:      "AC Frequency",
			},
		),
		pv1Volt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv1",
				Name:      "volt",
				Help:      "PV1 Voltage",
			},
		),
		pv1Ampere: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv1",
				Name:      "ampere",
				Help:      "PV1 Current",
			},
		),
		pv1Watt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv1",
				Name:      "watt",
				Help:      "PV1 Power",
			},
		),
		pv2Volt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv2",
				Name:      "volt",
				Help:      "PV2 Voltage",
			},
		),
		pv2Ampere: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv2",
				Name:      "ampere",
				Help:      "PV2 Current",
			},
		),
		pv2Watt: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "pv2",
				Name:      "watt",
				Help:      "PV2 Power",
			},
		),
		energyGeneratedTotal: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "energy_generated",
				Name:      "total_kwh",
				Help:      "Energy Generated Total",
			},
		),
		energyGeneratedToday: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "energy_generated",
				Name:      "today_kwh",
				Help:      "Energy Generated Today",
			},
		),
		energyExportedTotal: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "energy_exported",
				Name:      "total_kwh",
				Help:      "Energy Exported Total",
			},
		),
		energyImportedTotal: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "energy_imported",
				Name:      "total_kwh",
				Help:      "Energy Imported Total",
			},
		),
		powerExported: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Namespace: "solax",
				Subsystem: "power_exported",
				Name:      "watt",
				Help:      "Power Exported",
			},
		),
	}

	registry.MustRegister(
		metrics.infoSN,
		metrics.infoVer,
		metrics.scrapeSeconds,
		metrics.temp,
		metrics.acVolt,
		metrics.acAmpere,
		metrics.acWatt,
		metrics.acFreq,
		metrics.pv1Volt,
		metrics.pv1Ampere,
		metrics.pv1Watt,
		metrics.pv2Volt,
		metrics.pv2Ampere,
		metrics.pv2Watt,
		metrics.energyGeneratedTotal,
		metrics.energyGeneratedToday,
		metrics.energyExportedTotal,
		metrics.energyImportedTotal,
		metrics.powerExported,
	)
	// Register standard Go metrics to the custom registry
	registry.MustRegister(
		collectors.NewGoCollector(collectors.WithGoCollectorRuntimeMetrics()),
		collectors.NewProcessCollector(
			collectors.ProcessCollectorOpts{
				// Default values
				PidFn:        nil,
				Namespace:    "",
				ReportErrors: true,
			},
		),
	)

	return registry, metrics
}

func (m *Metrics) Update(data data.Parsed) {
	m.infoSN.With(prometheus.Labels{"SN": data.InfoSN}).Set(1)
	m.infoVer.With(prometheus.Labels{"Ver": data.InfoVer}).Set(1)
	m.scrapeSeconds.Set(data.ScrapeSeconds)

	m.temp.Set(data.Temp)

	m.acVolt.Set(data.ACVolt)
	m.acAmpere.Set(data.ACAmpere)
	m.acWatt.Set(data.ACWatt)
	m.acFreq.Set(data.ACFreq)

	m.pv1Volt.Set(data.PV1Volt)
	m.pv1Ampere.Set(data.PV1Ampere)
	m.pv1Watt.Set(data.PV1Watt)

	m.pv2Volt.Set(data.PV2Volt)
	m.pv2Ampere.Set(data.PV2Ampere)
	m.pv2Watt.Set(data.PV2Watt)

	m.energyGeneratedTotal.Set(data.EnergyGeneratedTotal)
	m.energyGeneratedToday.Set(data.EnergyGeneratedToday)
	m.energyExportedTotal.Set(data.EnergyExportedTotal)
	m.energyImportedTotal.Set(data.EnergyImportedTotal)
	m.powerExported.Set(data.PowerExported)
}
