package metrics

import (
	"reflect"
	"strings"
	"testing"

	prom_client "github.com/prometheus/client_model/go"
	"gitlab.com/yellowhat-labs/solax-exporter/internal/data"
)

func getMetrics(t *testing.T) (*Metrics, []*prom_client.MetricFamily) {
	t.Helper()

	registry, metrics := NewMetrics()

	promGather, err := registry.Gather()
	if err != nil {
		t.Errorf("Error retrieving metrics, got ‘%v’", err)
	}

	return metrics, promGather
}

func TestMetricsNames_SolaxGauge(t *testing.T) {
	t.Parallel()

	metricsExpected := []string{
		"solax_ac_ampere",
		"solax_ac_freq_hz",
		"solax_ac_volt",
		"solax_ac_watt",
		"solax_celsius",
		"solax_energy_exported_total_kwh",
		"solax_energy_generated_today_kwh",
		"solax_energy_generated_total_kwh",
		"solax_energy_imported_total_kwh",
		"solax_power_exported_watt",
		"solax_pv1_ampere",
		"solax_pv1_volt",
		"solax_pv1_watt",
		"solax_pv2_ampere",
		"solax_pv2_volt",
		"solax_pv2_watt",
		"solax_scrape_seconds",
	}

	var metricsRead []string

	_, promGather := getMetrics(t)
	for _, metric := range promGather {
		name := metric.GetName()
		if strings.HasPrefix(name, "solax_") && metric.GetType() == prom_client.MetricType_GAUGE {
			metricsRead = append(metricsRead, name)
		}
	}

	if !reflect.DeepEqual(metricsExpected, metricsRead) {
		t.Errorf("Metrics names are different:")
		t.Errorf(" - Ref: %v", metricsExpected)
		t.Errorf(" - New: %v", metricsRead)
	}
}

func TestMetricsNames_Prometheus(t *testing.T) {
	t.Parallel()

	metricsExpected := []string{
		"go_gc_duration_seconds",
		"go_gc_gogc_percent",
		"go_gc_gomemlimit_bytes",
		"go_goroutines",
		"go_info",
		"go_memstats_alloc_bytes",
		"go_memstats_alloc_bytes_total",
		"go_memstats_buck_hash_sys_bytes",
		"go_memstats_frees_total",
		"go_memstats_gc_sys_bytes",
		"go_memstats_heap_alloc_bytes",
		"go_memstats_heap_idle_bytes",
		"go_memstats_heap_inuse_bytes",
		"go_memstats_heap_objects",
		"go_memstats_heap_released_bytes",
		"go_memstats_heap_sys_bytes",
		"go_memstats_last_gc_time_seconds",
		"go_memstats_mallocs_total",
		"go_memstats_mcache_inuse_bytes",
		"go_memstats_mcache_sys_bytes",
		"go_memstats_mspan_inuse_bytes",
		"go_memstats_mspan_sys_bytes",
		"go_memstats_next_gc_bytes",
		"go_memstats_other_sys_bytes",
		"go_memstats_stack_inuse_bytes",
		"go_memstats_stack_sys_bytes",
		"go_memstats_sys_bytes",
		"go_sched_gomaxprocs_threads",
		"go_threads",
		"process_cpu_seconds_total",
		"process_max_fds",
		"process_network_receive_bytes_total",
		"process_network_transmit_bytes_total",
		"process_open_fds",
		"process_resident_memory_bytes",
		"process_start_time_seconds",
		"process_virtual_memory_bytes",
		"process_virtual_memory_max_bytes",
	}

	var metricsRead []string

	_, promGather := getMetrics(t)
	for _, metric := range promGather {
		name := metric.GetName()
		if !strings.HasPrefix(name, "solax_") {
			metricsRead = append(metricsRead, name)
		}
	}

	if !reflect.DeepEqual(metricsExpected, metricsRead) {
		t.Errorf("Metrics names are different:")
		t.Errorf(" - Ref: %v", metricsExpected)
		t.Errorf(" - New: %v", metricsRead)
	}
}

func TestUpdate(t *testing.T) {
	t.Parallel()

	fakeData := data.Parsed{
		InfoSN:               "SerialNumber",
		InfoVer:              "3.003.02",
		ScrapeSeconds:        0,
		Temp:                 1,
		ACVolt:               2,
		ACAmpere:             3,
		ACWatt:               4,
		ACFreq:               5,
		PV1Volt:              6,
		PV1Ampere:            7,
		PV1Watt:              8,
		PV2Volt:              9,
		PV2Ampere:            10,
		PV2Watt:              11,
		EnergyGeneratedTotal: 12,
		EnergyGeneratedToday: 13,
		EnergyExportedTotal:  14,
		EnergyImportedTotal:  15,
		PowerExported:        16,
	}

	m, _ := getMetrics(t)

	m.Update(fakeData)
}
