package data

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/getkin/kin-openapi/routers/gorillamux"
)

func TestToSigned(t *testing.T) {
	t.Parallel()

	tests := []struct {
		input    float64
		expected float64
	}{
		{-65_536, -65_536},
		{-64_386, -64_386},
		{-100, -100},
		{0, 0},
		{123.12, 123.12},
		{150, 150},
		{math.MaxInt16 - 1, 32_766},
		{65_536, 0},
		{math.MaxInt16 + 1, -32_768},
		// Values from solax
		{64_386, -1150},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%.2f", test.input), func(t *testing.T) {
			t.Parallel()

			val := toSigned(test.input)

			if math.Abs(val-test.expected) > 1e-9 {
				t.Errorf("Expected '%.3f', got '%.3f'", test.expected, val)
			}
		})
	}
}

func TestPackU16(t *testing.T) {
	t.Parallel()

	tests := []struct {
		i        int
		j        int
		expected float64
	}{
		{-10, 0, -10 + 2*math.MaxInt + 2},
		{-1, 0, 1 + 2*math.MaxInt},
		{0, 0, 0},
		{0, 1, 2*math.MaxInt16 + 2},
		{1, 1, 1 + 2*math.MaxInt16 + 2},
		{math.MaxInt32, math.MaxInt32, 140_739_635_773_439},
		{math.MaxInt32 + 1, math.MaxInt32, 140_739_635_773_440},
		{math.MaxInt32 + 1, math.MaxInt32 + 1, 140_739_635_838_976},
		{math.MaxInt32 + 10, math.MaxInt32 + 1, 140_739_635_838_985},
		// Values from solax
		{6_985, 11, 727_881},
		{23_768, 1, 89_304},
		{23_828, 20, 1_334_548},
		{42_751, 13, 894_719},
		{44_168, 1, 109_704},
		{49_046, 24, 1_621_910},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%d_%d", test.i, test.j), func(t *testing.T) {
			t.Parallel()

			val := packU16(test.i, test.j)

			if math.Abs(val-test.expected) > 1e-9 {
				t.Errorf("Expected '%.3f', got '%.3f'", test.expected, val)
			}
		})
	}
}

func mockServer() *httptest.Server {
	loader := openapi3.NewLoader()

	doc, err := loader.LoadFromFile("../../docs/openapi.yaml")
	if err != nil {
		log.Fatalf("doc load file: %s", err)
	}

	if err = doc.Validate(loader.Context); err != nil {
		log.Fatalf("doc validation: %s", err)
	}

	router, err := gorillamux.NewRouter(doc)
	if err != nil {
		log.Fatalf("Creating router: %s", err)
	}

	server := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, req *http.Request) {
		route, pathParams, err := router.FindRoute(req)
		if err != nil {
			log.Fatalf("Find route: %s", err)
		}

		requestValidationInput := &openapi3filter.RequestValidationInput{
			Request:    req,
			PathParams: pathParams,
			Route:      route,
		}
		if err = openapi3filter.ValidateRequest(req.Context(), requestValidationInput); err != nil {
			log.Fatalf("Validate request %s", err)
		}

		responses := route.PathItem.GetOperation(route.Method).Responses
		response := responses.Map()["200"].Value.Content["application/json"].Example

		responseJSON, err := json.Marshal(response)
		if err != nil {
			log.Fatalf("Response to json: %s", err)
		}

		_, err = writer.Write(responseJSON)
		if err != nil {
			log.Fatalf("Write response: %s", err)
		}
	}))

	return server
}

func compareParsed(t *testing.T, p1, p2 Parsed) {
	t.Helper()

	k := reflect.TypeOf(p1)
	v1 := reflect.ValueOf(p1)
	v2 := reflect.ValueOf(p2)

	for i := range v1.NumField() {
		fieldName := k.Field(i).Name
		field1 := v1.Field(i)
		field2 := v2.Field(i)

		switch field1.Kind() { //nolint:exhaustive // error on other type
		case reflect.String:
			if field1.String() != field2.String() {
				t.Errorf("Field '%s': expected '%s', got '%s'", fieldName, field1.String(), field2.String())
			}
		case reflect.Float64:
			if math.Abs(field1.Float()-field2.Float()) > 1.0e-6 {
				t.Errorf("Field '%s': expected '%f', got '%f'", fieldName, field1.Float(), field2.Float())
			}
		default:
			t.Error("Other type not handled")
		}
	}
}

func TestGet(t *testing.T) {
	t.Parallel()

	expected := Parsed{
		InfoSN:               "SerialNumber",
		InfoVer:              "3.003.02",
		ScrapeSeconds:        0.1,
		Temp:                 31,
		ACVolt:               230.4,
		ACAmpere:             3.4,
		ACWatt:               733,
		ACFreq:               49.99,
		PV1Volt:              250.2,
		PV1Ampere:            2.9,
		PV1Watt:              743,
		PV2Volt:              0,
		PV2Ampere:            0,
		PV2Watt:              0,
		EnergyGeneratedTotal: 8857.7,
		EnergyGeneratedToday: 0.7,
		EnergyExportedTotal:  7243.37,
		EnergyImportedTotal:  12999.11,
		PowerExported:        -427,
	}

	server := mockServer()
	defer server.Close()

	data, err := Get(server.URL, "SerialNumber")
	if err != nil {
		t.Errorf("Error in getting data, got `%s`", err)
	}

	// It is variable
	data.ScrapeSeconds = 0.1

	compareParsed(t, expected, data)
}

func TestUnavailable(t *testing.T) {
	t.Parallel()

	_, err := Get("http://10.0.0.1", "aaa")
	if err == nil {
		t.Errorf("This test should fail but it succeeded")
	}
}
