package data

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"time"
)

func toSigned(val float64) float64 {
	if val > math.MaxInt16 {
		val -= math.Pow(2, 16)
	}

	return val
}

func packU16(i int, j int) float64 {
	return float64(uint(i) + (uint(j) << 16)) // #nosec G115
}

type RealTimeData struct {
	SN          string          `json:"sn"`
	Ver         string          `json:"ver"`
	Type        int             `json:"type"`
	Data        [100]int        `json:"data"`
	Information [10]interface{} `json:"information"`
}

type Parsed struct {
	InfoSN               string
	InfoVer              string
	ScrapeSeconds        float64
	Temp                 float64
	ACVolt               float64
	ACAmpere             float64
	ACWatt               float64
	ACFreq               float64
	PV1Volt              float64
	PV1Ampere            float64
	PV1Watt              float64
	PV2Volt              float64
	PV2Ampere            float64
	PV2Watt              float64
	EnergyGeneratedTotal float64
	EnergyGeneratedToday float64
	EnergyExportedTotal  float64
	EnergyImportedTotal  float64
	PowerExported        float64
}

func parse(dataIn RealTimeData, elapsed time.Duration) Parsed {
	return Parsed{
		InfoSN:               dataIn.SN,
		InfoVer:              dataIn.Ver,
		ScrapeSeconds:        elapsed.Seconds(),
		Temp:                 float64(dataIn.Data[39]),
		ACVolt:               float64(dataIn.Data[0]) * 0.1,
		ACAmpere:             float64(dataIn.Data[1]) * 0.1,
		ACWatt:               float64(dataIn.Data[2]),
		ACFreq:               float64(dataIn.Data[9]) * 0.01,
		PV1Volt:              float64(dataIn.Data[3]) * 0.1,
		PV1Ampere:            float64(dataIn.Data[5]) * 0.1,
		PV1Watt:              float64(dataIn.Data[7]),
		PV2Volt:              float64(dataIn.Data[4]) * 0.1,
		PV2Ampere:            float64(dataIn.Data[6]) * 0.1,
		PV2Watt:              float64(dataIn.Data[8]),
		EnergyGeneratedTotal: packU16(dataIn.Data[11], dataIn.Data[12]) * 0.1,
		EnergyGeneratedToday: float64(dataIn.Data[13]) * 0.1,
		EnergyExportedTotal:  packU16(dataIn.Data[50], dataIn.Data[51]) * 0.01,
		EnergyImportedTotal:  packU16(dataIn.Data[52], dataIn.Data[53]) * 0.01,
		PowerExported:        toSigned(float64(dataIn.Data[48])),
	}
}

func Get(url string, pwd string) (Parsed, error) {
	request, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodPost,
		url,
		bytes.NewBufferString("optType=ReadRealTimeData&pwd="+pwd),
	)
	if err != nil {
		return Parsed{}, fmt.Errorf("error http.NewRequest: %w", err)
	}

	request.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	start := time.Now()

	response, err := client.Do(request)
	if err != nil {
		return Parsed{}, fmt.Errorf("error client.Do: %w", err)
	}
	defer response.Body.Close()

	elapsed := time.Since(start)

	decoder := json.NewDecoder(response.Body)
	decoder.DisallowUnknownFields()

	var realTimeData RealTimeData

	err = decoder.Decode(&realTimeData)
	if err != nil {
		return Parsed{}, fmt.Errorf("error decoder.Decode: %w", err)
	}

	data := parse(realTimeData, elapsed)

	return data, nil
}
