FROM docker.io/golang:1.24.1@sha256:fa145a3c13f145356057e00ed6f66fbd9bf017798c9d7b2b8e956651fe4f52da AS build

# Create statically linked executable
ARG CGO_ENABLED=0

WORKDIR /app

# Cache deps before building
COPY go.mod go.sum ./
RUN go mod download \
 && go mod verify

COPY internal ./internal
COPY main.go ./
 
RUN go build -o /solax-exporter

FROM scratch

COPY --from=build /solax-exporter /solax-exporter

ENTRYPOINT ["/solax-exporter"]
