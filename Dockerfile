FROM docker.io/golang:1.24.1@sha256:c5adecdb7b3f8c5ca3c88648a861882849cc8b02fed68ece31e25de88ad13418 AS build

# Create statically linked executable
ARG CGO_ENABLED=0

WORKDIR /app

# Cache deps before building
COPY go.mod go.sum ./
RUN go mod download \
 && go mod verify

COPY internal ./internal
COPY main.go ./
 
RUN go build -o /solax-exporter

FROM scratch

COPY --from=build /solax-exporter /solax-exporter

ENTRYPOINT ["/solax-exporter"]
