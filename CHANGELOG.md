## [1.1.13](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.12...v1.1.13) (2025-03-05)


### Bug Fixes

* **deps:** update all non-major dependencies ([4ac7243](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4ac72435384c0590e83cd70d3340bc4c165cff29))


### Chore

* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.10 ([3c2741a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3c2741adfb9c4720a5fb8e3b4c85801e27d8e396))
* **deps:** update docker.io/golang:1.24.0 docker digest to 3f74443 ([a11cabb](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a11cabb791bec265e00abb69e9cfec4bce29d99f))
* **deps:** update docker.io/golang:1.24.0 docker digest to 5255fad ([02d0495](https://gitlab.com/yellowhat-labs/solax-exporter/commit/02d049547f3f797d01e051e0e946e35167a3449f))
* **deps:** update docker.io/golang:1.24.0 docker digest to cd0c949 ([3f92270](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3f92270c4366a440e3639ff50c82fcffa601c8e6))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.64.6 ([e050315](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e05031571f354785b4d0290cfed87885464a1cf4))

## [1.1.12](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.11...v1.1.12) (2025-02-20)


### Bug Fixes

* **deps:** update all non-major dependencies ([861d165](https://gitlab.com/yellowhat-labs/solax-exporter/commit/861d165b7f52b1125a30ec94dee8381b0473512c))


### Chore

* **deps:** update all non-major dependencies ([399861d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/399861d98361b7b31fc541c9781f5b6a7fa76ffc))
* **deps:** update all non-major dependencies ([345c502](https://gitlab.com/yellowhat-labs/solax-exporter/commit/345c502a7c3a50b6857ced7039d26fba3745cb5a))
* **deps:** update all non-major dependencies ([478a024](https://gitlab.com/yellowhat-labs/solax-exporter/commit/478a0243930dfa6805142cbb4c4ed43a4b1a5a4d))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.7 ([0b55c9a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0b55c9a0a6a89bad670704ed54a8c9180e52eca7))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.8 ([755fd6f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/755fd6fc9fe9c06654fdec4f08557036bf79fa1d))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.9 ([8b43b91](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8b43b9143c121ab5fa258c76cf55dc02144609cb))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.7 ([6d1b0a9](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6d1b0a9ac8b3973579f0a00e8c9419984e2b2dad))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.64.5 ([4b75d15](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4b75d1589a354ed1c54b874665a52f6bb4fbe126))


### Continuous Integration

* **renovate:** sync ([0d09c57](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0d09c5734333e42a18a606de4489ce292915831d))

## [1.1.11](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.10...v1.1.11) (2025-02-09)


### Bug Fixes

* **deps:** update all non-major dependencies ([33d9de8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/33d9de8a1ab359c208a3e8cb647ec8ddbe8f8f5c))


### Chore

* **deps:** update all non-major dependencies ([6598465](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6598465fe07f743c7d6ff2809088eaf7acd4c15a))
* **deps:** update all non-major dependencies ([7cb9b70](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7cb9b707b5787013e9458084a1cd124475ea80a3))
* **deps:** update all non-major dependencies ([85d0547](https://gitlab.com/yellowhat-labs/solax-exporter/commit/85d05479784f5b3884d9071fdf4bb3af4ecbf129))
* **deps:** update all non-major dependencies ([053536d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/053536db5c4b9048287318a0393690f268d69a91))
* **deps:** update all non-major dependencies ([1f6f725](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1f6f72515288872925d4ebfb2deb2a912c6e19c8))
* **deps:** update all non-major dependencies ([a76ae04](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a76ae040f6c3578a6a72289a15094985bede096c))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.0.1 ([dab6cc9](https://gitlab.com/yellowhat-labs/solax-exporter/commit/dab6cc9d87215ab07527d0ee7822fcc5ae3c21ba))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.0.2 ([93f8566](https://gitlab.com/yellowhat-labs/solax-exporter/commit/93f8566d62ad7793d3ec6c154a369981a4c90e44))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.0 ([0c9722f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0c9722f60c34022a805b52a08660d458bd2dfdcb))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.1 ([9bfa818](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9bfa8188e75d752139f77dd067bd5ce53eab7088))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.1.2 ([6a12c0c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6a12c0c880d8984df5a6457dd88312b6cabf676c))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.2.0 ([8b36976](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8b36976055321baf6ec32e49ec07538949495f38))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.2.1 ([351f643](https://gitlab.com/yellowhat-labs/solax-exporter/commit/351f6434adde7c35518ffd6978da27628c42d17f))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.3.0 ([7b50b5b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7b50b5b4107de2ad52167cd910ae1953e93300cc))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.3 ([63c28ce](https://gitlab.com/yellowhat-labs/solax-exporter/commit/63c28ce0d8e6f237faea3b140f35cd55c7900a52))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.4 ([71b10db](https://gitlab.com/yellowhat-labs/solax-exporter/commit/71b10db4733a80012dfbcd7db4abc7c6f62342d0))
* **deps:** update dependency yellowhat-labs/bootstrap to v2.4.6 ([1c82273](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1c822737e58b1a2aa94efa9044c1e3755cfa5f58))
* **deps:** update dependency yellowhat-labs/utils to v1.12.3 ([268b0f8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/268b0f8523b94edde0c6f4b733e2029e0b1b6b69))
* **deps:** update dependency yellowhat-labs/utils to v1.12.4 ([1795473](https://gitlab.com/yellowhat-labs/solax-exporter/commit/17954739fa6d2a884e2c4570678347dfe5345300))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.4 ([c5da8bb](https://gitlab.com/yellowhat-labs/solax-exporter/commit/c5da8bbe24d17b14a718511d6cb260168785320f))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.5 ([57e7556](https://gitlab.com/yellowhat-labs/solax-exporter/commit/57e75566fc736d6dedae990db71ce3404c31cdab))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.6 ([d920d92](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d920d92280c3e9b85631174b4771d9b414efad35))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.14.0 ([42cf080](https://gitlab.com/yellowhat-labs/solax-exporter/commit/42cf0808e0b97de9106f8713dbd621a061b5a98a))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.14.1 ([1329f8f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1329f8f05b47dd1b7cb822618a0b355c8c01fd02))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.14.2 ([7428d0e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7428d0e18b450c888721d3024961587a13c4b2b6))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.14.3 ([0553a85](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0553a85ddff840dadd3a4015b7ea6af981622c45))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.15.1 ([1eb6367](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1eb636702bed6107a1cf8c187ee0f5af03d9ab93))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.15.3 ([90a5931](https://gitlab.com/yellowhat-labs/solax-exporter/commit/90a5931e8b8563b1af403c06cb0d7de3b6cfc036))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.16.0 ([761b215](https://gitlab.com/yellowhat-labs/solax-exporter/commit/761b215656d82ae1831fe71d91c8c057f3704f90))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.16.1 ([ad56a6a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/ad56a6a14d7a7dec12f495e5a1b8fcc8bce4ba67))
* **deps:** update docker.io/golang docker tag to v1.23.3 ([a827cf2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a827cf2c77b5615588d1ef27c300d8c194cfc0dc))
* **deps:** update docker.io/golang docker tag to v1.23.4 ([deb3f96](https://gitlab.com/yellowhat-labs/solax-exporter/commit/deb3f96f3483a4df4c67f239b07013e7a0daa1eb))
* **deps:** update docker.io/golang:1.23.2 docker digest to 540d344 ([25d3ab4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/25d3ab481f1385a4ba094a07030ca3f26ec4c0cc))
* **deps:** update docker.io/golang:1.23.2 docker digest to ad5c126 ([f048461](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f04846167ed321f63766c05dd607fe2ea998bc44))
* **deps:** update docker.io/golang:1.23.2 docker digest to cc637ce ([81a01a4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/81a01a480f5b592408d91c42ad34a93d1673a187))
* **deps:** update docker.io/golang:1.23.3 docker digest to 017ec6b ([120ab6f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/120ab6f8e32c26e886c17d7e415418610ed644f6))
* **deps:** update docker.io/golang:1.23.3 docker digest to 73f06be ([002d70a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/002d70ab5c60e56f2c64b23d5a807a2dc81c006f))
* **deps:** update docker.io/golang:1.23.3 docker digest to 8956c08 ([8f89a17](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8f89a170753adf129ebbdb7348fe4c4bf47391e3))
* **deps:** update docker.io/golang:1.23.3 docker digest to b2ca381 ([8b0f84d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8b0f84da3b3c5f62fabef636809be03b04fd361b))
* **deps:** update docker.io/golang:1.23.3 docker digest to c2d828f ([668d302](https://gitlab.com/yellowhat-labs/solax-exporter/commit/668d302613a6d581ae84333100caec4622fcafc3))
* **deps:** update docker.io/golang:1.23.3 docker digest to e5ca199 ([23cfdbd](https://gitlab.com/yellowhat-labs/solax-exporter/commit/23cfdbdbd089558cde81189beb4daf543a4c76d8))
* **deps:** update docker.io/golang:1.23.3 docker digest to ee5f0ad ([e8d4206](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e8d4206148b12813d0b9a827d843e85537f7daad))
* **deps:** update docker.io/golang:1.23.4 docker digest to 3b1a7de ([2e51972](https://gitlab.com/yellowhat-labs/solax-exporter/commit/2e51972747b554ad92c92461f617aba39d66932c))
* **deps:** update docker.io/golang:1.23.4 docker digest to 574185e ([6829bb1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6829bb1d37311f54b7204951342c4b965031c989))
* **deps:** update docker.io/golang:1.23.4 docker digest to 7003184 ([a0a81c6](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a0a81c6925757e3c4580de076074c3f08da73004))
* **deps:** update docker.io/golang:1.23.5 docker digest to 8c10f21 ([51d0d6f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/51d0d6f51f70ce44e9c851dfcf534edca6c2af77))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.62.0 ([fbbe5b4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/fbbe5b4da0b1813e0463db5f54f8ea6e3d811ad3))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.62.2 ([b234d6c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/b234d6c0659e109f26f2c16c52161410c8155cc2))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.63.3 ([62e4cc2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/62e4cc2fcad91db30366d534a0d5c258ec979f11))
* **deps:** update docker.io/stoplight/prism docker tag to v5.12.1 ([1b90116](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1b90116dbb12ec4d04be3e3f1ba6b3fd878ed00a))
* **deps:** update module github.com/mailru/easyjson to v0.9.0 ([a307110](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a307110678c8c1cc46acdff073f3aae025b109ef))
* **deps:** update module github.com/prometheus/common to v0.60.1 ([1bfa56b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1bfa56b9fd4c9f4ff9878aeb739b791650bc58cb))
* **deps:** update module github.com/prometheus/common to v0.61.0 ([253a6de](https://gitlab.com/yellowhat-labs/solax-exporter/commit/253a6de70dedb6e9646b547df7620ad3b198ed4f))
* **deps:** update module golang.org/x/sys to v0.27.0 ([0afdef2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0afdef29adc25816eaa6e0cc178495ac28aed073))
* **deps:** update module golang.org/x/sys to v0.28.0 ([19a3fe3](https://gitlab.com/yellowhat-labs/solax-exporter/commit/19a3fe35b2ef17badd9c5fdbaa3d34b20573db5b))
* **deps:** update module golang.org/x/sys to v0.29.0 ([de4ef4a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/de4ef4ab466c81f035a23033b5b1bb732077bb7e))
* **deps:** update module google.golang.org/protobuf to v1.35.2 ([886ee02](https://gitlab.com/yellowhat-labs/solax-exporter/commit/886ee023b931d73ffccc88b0e28f0f8a8fcde228))
* **deps:** update module google.golang.org/protobuf to v1.36.0 ([d20a478](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d20a4780a0a6d0dc8ee4d35ee9d39e45f171ed1c))


### Continuous Integration

* **bootstrap:** 2.0.0 ([00480e7](https://gitlab.com/yellowhat-labs/solax-exporter/commit/00480e7f35213b0b41af1818b10a09f9246255d4))

## [1.1.10](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.9...v1.1.10) (2024-10-16)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.5 ([4cbb95d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4cbb95d4eb5b6cedf2badeff4bda0de01b349912))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.12.1 ([1b914a2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1b914a27eae6e6bb1b6c38fe9a7ac4ce9a83f4ad))
* **deps:** update dependency yellowhat-labs/utils to v1.12.2 ([61078b8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/61078b8108f383d040db1d3f0de2cba068d72544))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.13.6 ([5383c2b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/5383c2bec6f4ed5a0f2785de0deb319c562f5d78))
* **deps:** update docker.io/golang:1.23.2 docker digest to a7f2fc9 ([fc7dae3](https://gitlab.com/yellowhat-labs/solax-exporter/commit/fc7dae34043fcc659d70f0df821f941214d1500a))
* **deps:** update module github.com/klauspost/compress to v1.17.11 ([394c513](https://gitlab.com/yellowhat-labs/solax-exporter/commit/394c5139f2a9a3fc76da9a6b759b19511edf0b7b))
* **deps:** update module google.golang.org/protobuf to v1.35.1 ([969f51e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/969f51e3a05c0fb074329e141d7095108c08eb45))

## [1.1.9](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.8...v1.1.9) (2024-10-07)


### Bug Fixes

* **deps:** update module github.com/getkin/kin-openapi to v0.128.0 ([3162f4c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3162f4c0d406867266d6a7de19a273bd95b2fb59))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.5 ([2cb1d27](https://gitlab.com/yellowhat-labs/solax-exporter/commit/2cb1d27e8dd6fb614c2f0bb2566726fc60d53c0d))
* **deps:** update dependency yellowhat-labs/utils to v1.12.0 ([9bc2844](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9bc28443fa6262865645d8a332d3e68bbdfac5d0))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.13.5 ([59e9fbe](https://gitlab.com/yellowhat-labs/solax-exporter/commit/59e9fbedb9d839fd7184a735622daa02ed45dceb))
* **deps:** update docker.io/golang docker tag to v1.23.2 ([1f2f3ae](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1f2f3ae776cca068def935fe55b81217fe52894d))
* **deps:** update docker.io/golang:1.23.1 docker digest to 4f063a2 ([ab59dc7](https://gitlab.com/yellowhat-labs/solax-exporter/commit/ab59dc772dfb25324a0b454524a494588016cad6))
* **deps:** update docker.io/golang:1.23.1 docker digest to efa5904 ([9570086](https://gitlab.com/yellowhat-labs/solax-exporter/commit/957008667c875cd1eec1bdecaf4412f08743e927))
* **deps:** update module github.com/klauspost/compress to v1.17.10 ([65696d2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/65696d24061d0d1c0e547df02edf6ca98f9b14a7))
* **deps:** update module github.com/prometheus/common to v0.60.0 ([b0b3d4a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/b0b3d4a7d7b5c97080e776d848ec0f67c1179d18))
* **deps:** update module golang.org/x/sys to v0.26.0 ([e582525](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e582525f28a0f245004622a6441d40d093c28bce))


### Continuous Integration

* add govulncheck ([e17249b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e17249b00ef396e0566268c20c5a54b5ff362b3f))
* **semgrep:** skip dockerfile user rule ([4f650fd](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4f650fda5eb158fb01e9fc6e7f0e576e42cf2768))
* **semgre:** skip dockerfile user rule ([4b62b55](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4b62b55bad46aec5ca3c3d00b9fcd86e94a87f3b))

## [1.1.8](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.7...v1.1.8) (2024-09-18)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.4 ([77a82d2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/77a82d20bf0bf08eadcbd714c6cf1e0f753b2976))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.3 ([fdde1cf](https://gitlab.com/yellowhat-labs/solax-exporter/commit/fdde1cf80013e5caf48abcba0f8c00a1d24a7d39))
* **deps:** update dependency yellowhat-labs/utils to v1.11.4 ([d21f16d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d21f16defff5c35f4c5949e82e9278166c47acde))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.3 ([558476f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/558476f46eb289694ce15007a8f8d481e1321720))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.13.1 ([c3080be](https://gitlab.com/yellowhat-labs/solax-exporter/commit/c3080beae0681e5a6bdd8505fb91b8f15a8c6a04))
* **deps:** update docker.io/golang:1.23.1 docker digest to 2fe82a3 ([c73e274](https://gitlab.com/yellowhat-labs/solax-exporter/commit/c73e2740825769bfa6885a80a95be88d00296e61))
* **deps:** update docker.io/golang:1.23.1 docker digest to 4a3c2bc ([1b56f96](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1b56f962e8bcd64fd9b76581601a9cd60300ff53))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.61.0 ([9f8e1f5](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9f8e1f51b7723ffb14549210fe31d69129e3ac2b))
* **deps:** update docker.io/stoplight/prism docker tag to v5.11.2 ([bd4c258](https://gitlab.com/yellowhat-labs/solax-exporter/commit/bd4c2582332057861fb49d5501de37c8aa8dc6ec))

## [1.1.7](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.6...v1.1.7) (2024-09-06)


### Bug Fixes

* **build:** build container on tag ([074dfce](https://gitlab.com/yellowhat-labs/solax-exporter/commit/074dfceffcb08838ffc9c00ea0de9770fec4a604))


### Continuous Integration

* remove staticcheck as included in golangci-lint ([9b95797](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9b95797bc56e31931ae09f96ea7db5ab18e6d8cc))
* **renovate:** sync ([5a0666e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/5a0666e824646a71269c2455b04d6ce5a8020bc1))

## [1.1.6](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.5...v1.1.6) (2024-09-06)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.3 ([dba27f1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/dba27f16a429419d3ef9d0ebd6e3746cca905faf))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.11.1 ([87ae6b1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/87ae6b1bb0b932c5b36e124af76b72208eaacb05))
* **deps:** update dependency yellowhat-labs/utils to v1.11.2 ([e59614e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e59614e0b148c2c75261e90b1204844b7565af21))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.0 ([dff84b1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/dff84b1f82044f96e4803ff87932c38e35fb81fa))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.1 ([d0647be](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d0647be70a8083ac56dff20bffd793f1da471076))
* **deps:** update dependency zxilly/go-size-analyzer to v1.7.2 ([d682811](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d68281138d306e33421caeb995cab41c700708e9))
* **deps:** update docker.io/golang docker tag to v1.23.1 ([20d4cd1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/20d4cd10029aa11edd4a7659b8f0b24ca0e645f5))
* **deps:** update docker.io/golang:1.23.0 docker digest to 1a6db32 ([13468f2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/13468f22423a12a6407a42f5dd49f93a0cc43586))
* **deps:** update module github.com/prometheus/common to v0.56.0 ([a76ed25](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a76ed25f5db319f59e04e86e062c16ee81cebb3d))
* **deps:** update module github.com/prometheus/common to v0.57.0 ([a257769](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a2577699debb9304c75ebe8afab1bd0775ef343c))
* **deps:** update module github.com/prometheus/common to v0.58.0 ([50b748e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/50b748e3dba9d2dbcb11f48386f467c50c71a930))
* **deps:** update module github.com/prometheus/common to v0.59.1 ([219ea29](https://gitlab.com/yellowhat-labs/solax-exporter/commit/219ea2949558b97df88febaf9759f29d91eff0db))
* **deps:** update module golang.org/x/sys to v0.25.0 ([b10adc0](https://gitlab.com/yellowhat-labs/solax-exporter/commit/b10adc069f9b445647b98a3fc0c1710f47ebe2f4))
* **dockerfile:** COPY only required files ([3c19f29](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3c19f29c747cb1888b57d3b073ec2dfb86457b9b))
* refactor hack ([66a5787](https://gitlab.com/yellowhat-labs/solax-exporter/commit/66a5787ed44f40414cfef3a61aeed031a1569eb3))
* security-opt :z ([247d16d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/247d16d17a89e264e06e3fd8d8cbb8d1636c743c))

## [1.1.5](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.4...v1.1.5) (2024-08-23)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.2 ([1f6fed3](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1f6fed32fab1db9d770e161354ba5798d4a10b14))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.10.8 ([666cf08](https://gitlab.com/yellowhat-labs/solax-exporter/commit/666cf08e2176d1fbc537a0dc97bee5b930d959f4))
* **deps:** update dependency yellowhat-labs/utils to v1.11.0 ([e327834](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e327834322df62938ff3fb8e3bd4a4f3e43c89ee))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.2 ([10289b9](https://gitlab.com/yellowhat-labs/solax-exporter/commit/10289b917a94e0426d57b98fcc60442f83571541))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.3 ([5dd0c9a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/5dd0c9aa27a65b21252120297db7ab197274bf66))

## [1.1.4](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.3...v1.1.4) (2024-08-20)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.1 ([0867259](https://gitlab.com/yellowhat-labs/solax-exporter/commit/086725958b9c09e340417796901d5dacc7551c5f))


### Chore

* **deps:** update dependency dominikh/go-tools to v2024.1.1 ([319c455](https://gitlab.com/yellowhat-labs/solax-exporter/commit/319c45561c051520c2dc8cf5b30cd89a4e222f4f))
* **deps:** update dependency yellowhat-labs/utils to v1.10.1 ([e809be2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e809be280aa46f6f4d729720d3acbd40caed9455))
* **deps:** update dependency yellowhat-labs/utils to v1.10.2 ([0390b70](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0390b7028430893e51720e9f447c088c8d29c082))
* **deps:** update dependency yellowhat-labs/utils to v1.10.3 ([b53a827](https://gitlab.com/yellowhat-labs/solax-exporter/commit/b53a82764514023e5e88497cd4f389784e9c194e))
* **deps:** update dependency yellowhat-labs/utils to v1.10.4 ([68f3427](https://gitlab.com/yellowhat-labs/solax-exporter/commit/68f34279b672ae51c6331626f96e9a2a3c0b81e8))
* **deps:** update dependency yellowhat-labs/utils to v1.10.5 ([e7ea55b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e7ea55b6e8893aefc110eaa88b5eee0b3b5c6185))
* **deps:** update dependency yellowhat-labs/utils to v1.10.6 ([804f8a3](https://gitlab.com/yellowhat-labs/solax-exporter/commit/804f8a34f676df2600102da451e8c22b47aeb5ba))
* **deps:** update dependency yellowhat-labs/utils to v1.10.7 ([d3df5e7](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d3df5e709be038f825c6be47a8b32f3a78f8e50b))
* **deps:** update dependency zxilly/go-size-analyzer to v1.6.3 ([f100a1d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f100a1dc4c1d9ce815300e4e21edbe0cadf3d3c1))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.12.1 ([0b8c12a](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0b8c12a57678b0c13d8f7fcf31724861724f371b))
* **deps:** update docker.io/golang:1.23.0 docker digest to 613a108 ([c44d848](https://gitlab.com/yellowhat-labs/solax-exporter/commit/c44d84828e19dc326db2a8368563ad4d44276bc3))


### Continuous Integration

* fix renovate ([13e31bc](https://gitlab.com/yellowhat-labs/solax-exporter/commit/13e31bcdf86241f427d871195ce571dbcb05e9a8))

## [1.1.3](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.2...v1.1.3) (2024-08-14)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.20.0 ([bf240b1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/bf240b109e99e7bc6d096c5e6134b7bfcd3efa95))


### Continuous Integration

* remove release stage ([fea5886](https://gitlab.com/yellowhat-labs/solax-exporter/commit/fea5886500a3d873b5adb45e599731214369a3bf))
* **renovate:** fix ([744359c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/744359c8255b61712ebb5e35097d35b5adbed8b4))
* **test:** no need for artifacts ([4aa1154](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4aa11547b21abad8582ec3395a3e2b5dee631a0c))


### Chore

* **deps:** update dependency dominikh/go-tools to v2024 ([644f70d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/644f70ddb297395ec90778be45ddbc3d1a0202d5))
* **deps:** update dependency yellowhat-labs/utils to v1.10.0 ([adf2c71](https://gitlab.com/yellowhat-labs/solax-exporter/commit/adf2c71e72547a475efd22bc4f9adb6c052c7bc9))
* **deps:** update dependency zxilly/go-size-analyzer to v1.6.1 ([3a6d47d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3a6d47d25210aa539fb5a59bcdd8f9f835819f99))
* **deps:** update dependency zxilly/go-size-analyzer to v1.6.2 ([4c20f97](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4c20f97a8284075b5985b57d62e3973f0373f6c5))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.12.0 ([5f26926](https://gitlab.com/yellowhat-labs/solax-exporter/commit/5f26926c320d0a01d139baab8986c88257280fa3))
* **deps:** update docker.io/golang docker tag to v1.22.6 ([fbbacfb](https://gitlab.com/yellowhat-labs/solax-exporter/commit/fbbacfb4ed14cf741b567ca9aa06012d74d5a930))
* **deps:** update docker.io/golang docker tag to v1.23.0 ([870aa22](https://gitlab.com/yellowhat-labs/solax-exporter/commit/870aa22e4bef87cd63cb2365f5b4a3d4fd720558))
* **deps:** update docker.io/golang:1.22.6 docker digest to 305aae5 ([f47086f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f47086f3acae440212778405c1744e05c157774f))
* **deps:** update docker.io/golang:1.22.6 docker digest to bb9d8c4 ([22ea376](https://gitlab.com/yellowhat-labs/solax-exporter/commit/22ea3768275c0e48a4eb12f5172ccd342d28be29))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.60.1 ([72626ae](https://gitlab.com/yellowhat-labs/solax-exporter/commit/72626aec1bae577626967a77032d6a13466b25e6))
* **deps:** update docker.io/stoplight/prism docker tag to v5.11.1 ([a0c99ae](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a0c99ae8d766cca57ecdc41286acd7cbbf059fc8))
* **deps:** update module golang.org/x/sys to v0.23.0 ([8c3fb72](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8c3fb724b369ff977504d1c01717ce89da7dd0fd))
* **deps:** update module golang.org/x/sys to v0.24.0 ([f9c3cc5](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f9c3cc51c5e6af9a952d754f7fae861f3107e782))

## [1.1.2](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.1...v1.1.2) (2024-07-31)


### Bug Fixes

* **deps:** update module github.com/getkin/kin-openapi to v0.127.0 ([6374eeb](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6374eeb6390c87cefeb487ba7054877fdfe82bc7))


### Continuous Integration

* add go-size-analyzer ([d98a67c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d98a67cdd76df236f83f73b57cd1d8fa141dc16d))
* comment on gsa ([41214ca](https://gitlab.com/yellowhat-labs/solax-exporter/commit/41214ca83562847c0599fb0dd819fa8007c6ee70))
* renovate fix ([62503ba](https://gitlab.com/yellowhat-labs/solax-exporter/commit/62503ba52978938a7005aba673308875594c1a61))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.9.0 ([1d159b2](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1d159b20a30776a234f01e2c46ec7d7a699aa126))
* **deps:** update dependency yellowhat-labs/utils to v1.9.1 ([f4a0691](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f4a06911a72f48b569e650f63ae58e073a7f9269))
* **deps:** update dependency zxilly/go-size-analyzer to v1.4.2 ([6057d63](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6057d631f26f639f7c49427d06f1e141fc07ada2))
* **deps:** update dependency zxilly/go-size-analyzer to v1.4.3 ([f221624](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f221624c9df0e99839a01529a51a42f3c13a1850))
* **deps:** update dependency zxilly/go-size-analyzer to v1.5.0 ([d027874](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d027874eebd0c5407bc246db99add82266e8daf7))
* **deps:** update dependency zxilly/go-size-analyzer to v1.5.3 ([df92b3b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/df92b3b3e134da371456c6e39f6b656656433c7c))
* **deps:** update dependency zxilly/go-size-analyzer to v1.5.4 ([1669078](https://gitlab.com/yellowhat-labs/solax-exporter/commit/166907853827a6e3211e0cc7b65a9e27199e6476))
* **deps:** update dependency zxilly/go-size-analyzer to v1.6.0 ([22b65c4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/22b65c4b2629cb979874ddb9ff5e0f698821691c))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.11.1 ([7498910](https://gitlab.com/yellowhat-labs/solax-exporter/commit/74989105ae5f4dc63c112d5f9166b8c48ae343cd))
* **deps:** update docker.io/golang:1.22.5 docker digest to 829eff9 ([9c625e8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9c625e8619511b68fad4750331d1fbb6b7143129))
* **deps:** update docker.io/golang:1.22.5 docker digest to 86a3c48 ([daede07](https://gitlab.com/yellowhat-labs/solax-exporter/commit/daede071351fcf16977665d32f0960065abd1141))
* **deps:** update docker.io/stoplight/prism docker tag to v5.11.0 ([81b2cd9](https://gitlab.com/yellowhat-labs/solax-exporter/commit/81b2cd9fd0b4304c21bdc9eea2e78e232f867eb7))

## [1.1.1](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.1.0...v1.1.1) (2024-07-09)


### Bug Fixes

* move parsing data to data ([d931a23](https://gitlab.com/yellowhat-labs/solax-exporter/commit/d931a2301718a7b6ca17289f65677994cc4e5e59))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.8.4 ([e592c3b](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e592c3becc9a27a9b4c5ad1177fcdc729ddb1bc5))


### Continuous Integration

* renovate fix ([8a9a7c8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8a9a7c85f85b6546e7c342712c300f6ecb8edb02))

# [1.1.0](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.0.18...v1.1.0) (2024-07-07)


### Features

* refactor to avoid global variables ([ff8529f](https://gitlab.com/yellowhat-labs/solax-exporter/commit/ff8529f0c761c79bf0ff059ac7418b2e7b07613d))


### deps

* **deps:** update module github.com/getkin/kin-openapi to v0.126.0 ([7e8925d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7e8925d928ed99eaaed1118f8955c96c86141bb5))


### Chore

* better Dockerfile caching ([411f746](https://gitlab.com/yellowhat-labs/solax-exporter/commit/411f74648ef9a991e1c33df3f75740ac0904e487))
* **deps:** update dependency yellowhat-labs/utils to v1.8.0 ([bf6750d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/bf6750d14363ac29dedb0ebe277e4d499dc443c5))
* **deps:** update dependency yellowhat-labs/utils to v1.8.1 ([0652940](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0652940f03dee910367e6250861bd7f4cc832bb5))
* **deps:** update dependency yellowhat-labs/utils to v1.8.2 ([06f51ec](https://gitlab.com/yellowhat-labs/solax-exporter/commit/06f51ecf3b99a55647950ec1d35b33c519e3825a))
* **deps:** update dependency yellowhat-labs/utils to v1.8.3 ([eadc191](https://gitlab.com/yellowhat-labs/solax-exporter/commit/eadc191df913cd29693256135fc5ece50b6aeb5d))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.10.1 ([5a98768](https://gitlab.com/yellowhat-labs/solax-exporter/commit/5a98768eef1e28d2cf9987f9a91c13e7e306af2f))
* **deps:** update docker.io/golang docker tag to v1.22.5 ([6bf0b60](https://gitlab.com/yellowhat-labs/solax-exporter/commit/6bf0b60cb4f3afa83d5353b9a0ae5b78c2057f64))
* **deps:** update docker.io/golang:1.22.4 docker digest to 0f76912 ([79c14a5](https://gitlab.com/yellowhat-labs/solax-exporter/commit/79c14a543689300654b0804ecb53731156973c3d))
* **deps:** update docker.io/golang:1.22.4 docker digest to 3589439 ([1b38dc6](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1b38dc64075e4f4911a691b0987caca3664714b8))
* **deps:** update docker.io/golang:1.22.4 docker digest to a0679ac ([db3444e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/db3444ee8df808ca04b2b12c3d66704e60205954))
* **deps:** update docker.io/golang:1.22.4 docker digest to a66eda6 ([252928c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/252928c6b451e48d76ce952ee64477e24e1b237c))
* **deps:** update docker.io/golang:1.22.4 docker digest to c2010b9 ([9bd7ef4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/9bd7ef4ad46cc90efa2dc979c33e058856fcfd9a))
* **deps:** update docker.io/golang:1.22.4 docker digest to c8736b8 ([8e9fd1c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8e9fd1c89c81c573c5c16b2d36e7b4b2edcb01fc))
* **deps:** update docker.io/golang:1.22.5 docker digest to fcae9e0 ([a456319](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a4563195a939ae3c9228f8db832c8aec764a426d))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.59.1 ([3a33792](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3a33792becab92470e4f14868066593c2b1929cd))
* **deps:** update docker.io/stoplight/prism docker tag to v5.8.2 ([cfb468e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/cfb468e3df7e0ca26349b3b6c84bd866c478e4fa))
* **deps:** update module github.com/getkin/kin-openapi to v0.125.0 ([731153d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/731153d0284663fa31cf16021267ef08137ca772))
* **deps:** update module github.com/prometheus/common to v0.55.0 ([3ba9f3d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/3ba9f3d01a62bdc510f455de72aa773ca30b78af))
* **deps:** update module golang.org/x/sys to v0.22.0 ([a9dd434](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a9dd4347e0ed63f7fef089adbf2044f8a1d0b605))
* **deps:** update module google.golang.org/protobuf to v1.34.2 ([82d7bfc](https://gitlab.com/yellowhat-labs/solax-exporter/commit/82d7bfcac383a991438b5fd727d7e1ef38e47f9d))


### Continuous Integration

* fix renovate ([f48fc5c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/f48fc5c8515b79cdb3492e4d857a92db5b42504f))
* **go-fmt:** recursive ([a2c0107](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a2c01076cb0eb220f6c0f42c90f2c2369144c2d8))
* **golangci-lint:** cleanup ([e79cca4](https://gitlab.com/yellowhat-labs/solax-exporter/commit/e79cca4e197a3b307c2bc433cf3f520d5d7abe77))
* **renovate:** fix ([1ee39bb](https://gitlab.com/yellowhat-labs/solax-exporter/commit/1ee39bbc664419379b3dcbb5ba0a46892d715b83))
* **test:** add coverage ([23f15d1](https://gitlab.com/yellowhat-labs/solax-exporter/commit/23f15d11f33328a40424f2a888ef6359f951e80f))


* Add LICENSE ([93a20ea](https://gitlab.com/yellowhat-labs/solax-exporter/commit/93a20eaeee23a9b9a72fc544fb17cca5109d4860))

## [1.0.18](https://gitlab.com/yellowhat-labs/solax-exporter/compare/v1.0.17...v1.0.18) (2024-06-05)


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.6.4 ([ce678c0](https://gitlab.com/yellowhat-labs/solax-exporter/commit/ce678c024753b8c6ae9292de9bff1c8413affef7))
* **deps:** update dependency yellowhat-labs/utils to v1.7.0 ([54fd070](https://gitlab.com/yellowhat-labs/solax-exporter/commit/54fd0706bbdeacfc530fa2a80bf9bbaa97f30df9))
* **deps:** update dependency yellowhat-labs/utils to v1.7.1 ([04cbf87](https://gitlab.com/yellowhat-labs/solax-exporter/commit/04cbf878e02da067430e9ec027354ff48d3c3363))
* **deps:** update dependency yellowhat-labs/utils to v1.7.2 ([010aeab](https://gitlab.com/yellowhat-labs/solax-exporter/commit/010aeab90888c52757128d60665d2ef27b7ed4cd))
* **deps:** update dependency yellowhat-labs/utils to v1.7.3 ([03f0ab8](https://gitlab.com/yellowhat-labs/solax-exporter/commit/03f0ab8b416eccc59831eb5d6c05a8fb9bb4655a))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.10.0 ([7bb9c73](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7bb9c73af8d03586289a5a4e916d69bdd2b18026))
* **deps:** update docker.io/golang:1.22.3 docker digest to 72e8f4d ([8e64349](https://gitlab.com/yellowhat-labs/solax-exporter/commit/8e64349db3aceedd5d09fc0983cee997d0ee97b9))
* **deps:** update docker.io/golang:1.22.3 docker digest to 75b7aad ([4a8fa19](https://gitlab.com/yellowhat-labs/solax-exporter/commit/4a8fa194188b3a3c04c4e19e9882875eb868649f))
* **deps:** update docker.io/golang:1.22.3 docker digest to ab48cd7 ([31454ae](https://gitlab.com/yellowhat-labs/solax-exporter/commit/31454ae6bafda667087f3efd61886aa6b57a2aea))
* **deps:** update docker.io/golang:1.22.3 docker digest to f43c6f0 ([56a273e](https://gitlab.com/yellowhat-labs/solax-exporter/commit/56a273e225938ff3a6d8553694f105c70f7401ef))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.58.1 ([92257c6](https://gitlab.com/yellowhat-labs/solax-exporter/commit/92257c6a7d588a20505ebfb157c2aa8ddef4ea34))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.58.2 ([a6ce41d](https://gitlab.com/yellowhat-labs/solax-exporter/commit/a6ce41de1fbb459a0231519f847a73c13a2d5d95))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.59.0 ([0e107e9](https://gitlab.com/yellowhat-labs/solax-exporter/commit/0e107e90e750a94b554433e10231a2b7fb4108da))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.23.0 ([997e543](https://gitlab.com/yellowhat-labs/solax-exporter/commit/997e543db9a1006e4c14e19454875885c525e3e5))
* **deps:** update module github.com/prometheus/common to v0.54.0 ([7cba15c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7cba15cfd450e4e1a7409d044b7dae637e7c7969))
* **deps:** update module github.com/prometheus/procfs to v0.15.0 ([7dd758c](https://gitlab.com/yellowhat-labs/solax-exporter/commit/7dd758ca0034c97de4dd05d6f7738d54ad07fd44))
* **deps:** update module github.com/prometheus/procfs to v0.15.1 ([18e1100](https://gitlab.com/yellowhat-labs/solax-exporter/commit/18e1100401c2dba921de3c665f01d0d0ff67987e))
* **deps:** update module golang.org/x/sys to v0.21.0 ([50204f3](https://gitlab.com/yellowhat-labs/solax-exporter/commit/50204f3a0242ccb5881b994beb02fd35604c88b8))


### Bug Fixes

* **deps:** update docker.io/golang docker tag to v1.22.4 ([53c2da6](https://gitlab.com/yellowhat-labs/solax-exporter/commit/53c2da6b2a3ee132824dd921ff05935d31823b21))

## [1.0.17](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.16...v1.0.17) (2024-05-13)


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.6.2 ([(2de4764)](https://gitlab.com/yellowhat-labs/solax/commit/2de476438bb47689106ac9677c88381ef107e2b2))
* **deps:** update dependency yellowhat-labs/utils to v1.6.3 ([(0fe9de4)](https://gitlab.com/yellowhat-labs/solax/commit/0fe9de4f4159fb5f5f96f5b1e37e32089a2d02ad))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.12 ([(0e1c283)](https://gitlab.com/yellowhat-labs/solax/commit/0e1c283077e072f1d5789cb162196d680cd6b1f2))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.13 ([(7b6e933)](https://gitlab.com/yellowhat-labs/solax/commit/7b6e933c2a327adb5bf28eae5d5f658f5e3598f2))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.14 ([(80aea7d)](https://gitlab.com/yellowhat-labs/solax/commit/80aea7d35ed74b0a40c69dc70ac79a8020f6845b))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.15 ([(73a37c4)](https://gitlab.com/yellowhat-labs/solax/commit/73a37c4c747754e75682d75c17267905c9f4d789))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.16 ([(9acbed6)](https://gitlab.com/yellowhat-labs/solax/commit/9acbed6e7411cc787d842b10ac331f642865213d))
* **deps:** update docker.io/golang docker tag to v1.22.3 ([(54ae0fd)](https://gitlab.com/yellowhat-labs/solax/commit/54ae0fd10be98954c414f200c12289ba3048a0f3))
* **deps:** update docker.io/golang:1.22.2 docker digest to 1f47e5e ([(bfcd6ec)](https://gitlab.com/yellowhat-labs/solax/commit/bfcd6ecebaf01da5fa4249398780a3c462a2e499))
* **deps:** update docker.io/golang:1.22.2 docker digest to 450e382 ([(c11755a)](https://gitlab.com/yellowhat-labs/solax/commit/c11755a9919727e0bcd253b01010fec3b896ef1c))
* **deps:** update docker.io/golang:1.22.2 docker digest to 82f0d84 ([(6e3302f)](https://gitlab.com/yellowhat-labs/solax/commit/6e3302f6227dd8b522983fb714425ed0d936bde4))
* **deps:** update docker.io/golang:1.22.2 docker digest to d5302d4 ([(5ed0e4b)](https://gitlab.com/yellowhat-labs/solax/commit/5ed0e4b948fad1a0625924b58bcb7c39076b816e))
* **deps:** update docker.io/stoplight/prism docker tag to v5.8.1 ([(a01ea03)](https://gitlab.com/yellowhat-labs/solax/commit/a01ea033ad72899cf1492c0406f2489d737609df))
* **deps:** update module github.com/invopop/yaml to v0.3.1 ([(fcd1a22)](https://gitlab.com/yellowhat-labs/solax/commit/fcd1a229d29527d04c7a68a1b6ff0a27fe4e3819))
* **deps:** update module github.com/prometheus/common to v0.52.3 ([(5c214ec)](https://gitlab.com/yellowhat-labs/solax/commit/5c214ec50e84f437547ecec374e7cb23f22f8ae7))
* **deps:** update module github.com/prometheus/common to v0.53.0 ([(1ddeec7)](https://gitlab.com/yellowhat-labs/solax/commit/1ddeec7a9492cb9cd10bd875129afa17098baa0b))
* **deps:** update module github.com/prometheus/procfs to v0.14.0 ([(1aeb55a)](https://gitlab.com/yellowhat-labs/solax/commit/1aeb55a1ed1b1f1f32c2736fb2a6f1d2b187f743))
* **deps:** update module golang.org/x/sys to v0.20.0 ([(b5052d1)](https://gitlab.com/yellowhat-labs/solax/commit/b5052d1de85f183d77a12b48db0e7b0369ccdb06))
* **deps:** update module google.golang.org/protobuf to v1.34.1 ([(ac92771)](https://gitlab.com/yellowhat-labs/solax/commit/ac92771802283afde6ce0ab85ae548e9f315a9c8))
* **golangci-lint:** double timeout ([(af57e3c)](https://gitlab.com/yellowhat-labs/solax/commit/af57e3cffc97728f77f6a6f404f651043fc9bf75))


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.19.1 ([(7a3358a)](https://gitlab.com/yellowhat-labs/solax/commit/7a3358a544b0cf7e2436dfe95ab71afeeb925755))

## [1.0.16](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.15...v1.0.16) (2024-04-06)


* go.mod: 1.22 ([(fab113e)](https://gitlab.com/yellowhat-labs/solax/commit/fab113e2319045b537b24ca1051aa0db1127aee4))


### Bug Fixes

* **deps:** update module github.com/getkin/kin-openapi to v0.124.0 ([(ab2163a)](https://gitlab.com/yellowhat-labs/solax/commit/ab2163a88b03e196ae82fbaeb03bc19eb1bbadb3))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.6.1 ([(14b882e)](https://gitlab.com/yellowhat-labs/solax/commit/14b882e504c0805f983d6bea218d704600ab1687))
* **deps:** update module github.com/cespare/xxhash/v2 to v2.3.0 ([(21e08db)](https://gitlab.com/yellowhat-labs/solax/commit/21e08dba8497f2e9961f2007de216056b79d370d))
* **deps:** update module golang.org/x/sys to v0.19.0 ([(2d1fe19)](https://gitlab.com/yellowhat-labs/solax/commit/2d1fe1988c32164ebb41cd37c91fb8fb4d039aee))

## [1.0.15](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.14...v1.0.15) (2024-04-03)


### Bug Fixes

* **deps:** update docker.io/golang docker tag to v1.22.2 ([(6a45290)](https://gitlab.com/yellowhat-labs/solax/commit/6a452908d02797c29ca541945935588aac273d9a))


### Chore

* **deps:** pin docker.io/stoplight/prism docker tag to 08ed489 ([(98a3572)](https://gitlab.com/yellowhat-labs/solax/commit/98a35729c62d2237a674fd7658a3a1a51aff40f1))
* **deps:** update dependency yellowhat-labs/utils to v1.6.0 ([(985f3f8)](https://gitlab.com/yellowhat-labs/solax/commit/985f3f8337966cf8e6bcfd902cf528db18ec0da5))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.11 ([(3409940)](https://gitlab.com/yellowhat-labs/solax/commit/34099409fa99c17611cdcaabca3af05d838629fb))
* **deps:** update module github.com/prometheus/client_model to v0.6.1 ([(9576d05)](https://gitlab.com/yellowhat-labs/solax/commit/9576d05ea5a5b229396d3c2fa9381db446ebeb91))
* **deps:** update module github.com/prometheus/common to v0.52.2 ([(6ad2f89)](https://gitlab.com/yellowhat-labs/solax/commit/6ad2f89efe1390fd4bd7bac0000e56179bfeee95))
* **renovate:** fix ([(424bb29)](https://gitlab.com/yellowhat-labs/solax/commit/424bb290d36e665ae88d177ca9fd425c28234844))


### Continuous Integration

* add end-to-end test ([(bd036fc)](https://gitlab.com/yellowhat-labs/solax/commit/bd036fc499d74487d537a668fd77c2aaf0d1a297))
* move go-test linter -> test ([(6dd238e)](https://gitlab.com/yellowhat-labs/solax/commit/6dd238eb882c5e0132eda4ca4d9ccbdd0e7bcfdd))

## [1.0.14](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.13...v1.0.14) (2024-03-31)


### Bug Fixes

* **refactor:** add more linter ([(525a25c)](https://gitlab.com/yellowhat-labs/solax/commit/525a25cbbd3aa7abade46f01bade76830760f881))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.18 ([(43e9db8)](https://gitlab.com/yellowhat-labs/solax/commit/43e9db877db6b5f9ff80d87fadcf90507154aa0e))
* **deps:** update dependency yellowhat-labs/utils to v1.4.19 ([(c84620a)](https://gitlab.com/yellowhat-labs/solax/commit/c84620ad45fb0013f8c81f8bff61b2c4b54fd811))
* **deps:** update dependency yellowhat-labs/utils to v1.4.20 ([(dfdb923)](https://gitlab.com/yellowhat-labs/solax/commit/dfdb923440a088b88750d69e486eab0e7c932bfe))
* **deps:** update dependency yellowhat-labs/utils to v1.4.21 ([(8301d18)](https://gitlab.com/yellowhat-labs/solax/commit/8301d188e1a7bc6cca97c5b60174935b5bbc3e81))
* **deps:** update dependency yellowhat-labs/utils to v1.5.2 ([(59ac4ca)](https://gitlab.com/yellowhat-labs/solax/commit/59ac4ca8243ce53564118ef678a477c09b42ecd7))
* **deps:** update dependency yellowhat-labs/utils to v1.5.3 ([(fce9127)](https://gitlab.com/yellowhat-labs/solax/commit/fce9127ce1de36835b29ff89dd38ded2363947e5))
* **deps:** update docker.io/golang:1.22.1 docker digest to 0b55ab8 ([(20b1e65)](https://gitlab.com/yellowhat-labs/solax/commit/20b1e65af63e6ee714ca0820db5676e0a6ad5bc1))
* **deps:** update docker.io/golang:1.22.1 docker digest to 282dd13 ([(25561bd)](https://gitlab.com/yellowhat-labs/solax/commit/25561bdfc7ec8ec69653f3222cdc8caa5e789322))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.57.0 ([(e751512)](https://gitlab.com/yellowhat-labs/solax/commit/e751512cf5577489211a69b01dec5f99271a4687))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.57.1 ([(fbfc1f5)](https://gitlab.com/yellowhat-labs/solax/commit/fbfc1f50dd28508a2475931fc8fa793f3b5702b9))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.57.2 ([(7b910df)](https://gitlab.com/yellowhat-labs/solax/commit/7b910df15465863fd8b670c17c05647712441e5e))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.22.0 ([(77a1361)](https://gitlab.com/yellowhat-labs/solax/commit/77a136140fc480a94c9d07238d246702988aef67))
* **deps:** update go-openapi packages ([(8a26a55)](https://gitlab.com/yellowhat-labs/solax/commit/8a26a558606727db3df05f2d103d2dde173ecdb2))
* **deps:** update module github.com/prometheus/common to v0.51.0 ([(6deebc3)](https://gitlab.com/yellowhat-labs/solax/commit/6deebc334e230d39c5ceb5763e862bc6b5edcfbc))
* **deps:** update module github.com/prometheus/common to v0.51.1 ([(3ff5742)](https://gitlab.com/yellowhat-labs/solax/commit/3ff5742342422a8e34d4842471b4bbc386bf7245))

## [1.0.13](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.12...v1.0.13) (2024-3-7)


### Bug Fixes

* **deps:** update module github.com/prometheus/procfs to v0.13.0 ([(c19fb6e)](https://gitlab.com/yellowhat-labs/solax/commit/c19fb6ee0bbf419e4ed73b77622888ddb28580a8))


### Chore

* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.1 ([(cd0816c)](https://gitlab.com/yellowhat-labs/solax/commit/cd0816c1b24784db8f379e1456d81ec8b4eed192))
* **deps:** update module github.com/prometheus/common to v0.50.0 ([(1cabf85)](https://gitlab.com/yellowhat-labs/solax/commit/1cabf8597d9c9f73abc44f12abb0395d2ca44bd2))

## [1.0.12](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.11...v1.0.12) (2024-3-5)


### Bug Fixes

* **deps:** update docker.io/golang docker tag to v1.22.1 ([(0450add)](https://gitlab.com/yellowhat-labs/solax/commit/0450addd4c3b432587c65537a0867eb89fb35e0b))


### Chore

* **deps:** update module google.golang.org/protobuf to v1.33.0 ([(1321804)](https://gitlab.com/yellowhat-labs/solax/commit/1321804788ccb7f35059b663e64c843321c96d76))

## [1.0.11](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.10...v1.0.11) (2024-3-4)


### Bug Fixes

* **deps:** update module golang.org/x/sys to v0.18.0 ([(3f18424)](https://gitlab.com/yellowhat-labs/solax/commit/3f18424e5d86792615a01645c1045082bace5620))


### Chore

* **deps:** update go-openapi packages ([(60cc763)](https://gitlab.com/yellowhat-labs/solax/commit/60cc7631152ea7273e3208e30a1066128ff7d146))


### Continuous Integration

* **golangci-lint:** set timeout 10m ([(a31f436)](https://gitlab.com/yellowhat-labs/solax/commit/a31f436db06c88e12fe022a3d3d114412e08f3b7))

## [1.0.10](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.9...v1.0.10) (2024-3-1)


### Bug Fixes

* **deps:** update module github.com/prometheus/common to v0.49.0 ([(57c53b9)](https://gitlab.com/yellowhat-labs/solax/commit/57c53b989567a03d878e9691abb53264e9f0e56f))


### Chore

* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.10 ([(303849a)](https://gitlab.com/yellowhat-labs/solax/commit/303849a9e573e45ff72472d10bb230d13deec343))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.21.0 ([(822d126)](https://gitlab.com/yellowhat-labs/solax/commit/822d1264345b8b1ea496a13c5e1b5b54df70fc70))

## [1.0.9](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.8...v1.0.9) (2024-2-28)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_golang to v1.19.0 ([(be0a69c)](https://gitlab.com/yellowhat-labs/solax/commit/be0a69ccb703c5efa402e4ad0220edb9440a23e9))


### Chore

* **deps:** update dependency dominikh/go-tools to v2023.1.7 ([(cc561b7)](https://gitlab.com/yellowhat-labs/solax/commit/cc561b71bfdfeeba6576724fa748c9d5cb4ef6cf))
* **deps:** update dependency yellowhat-labs/utils to v1.4.15 ([(4aa69d7)](https://gitlab.com/yellowhat-labs/solax/commit/4aa69d7aab3c4ef22cbf73ff07ec90e5add0abc0))
* **deps:** update dependency yellowhat-labs/utils to v1.4.16 ([(4c704ab)](https://gitlab.com/yellowhat-labs/solax/commit/4c704abef0bef1d61fcea1f661303dc70ec5c7a7))
* **deps:** update dependency yellowhat-labs/utils to v1.4.17 ([(7092c2b)](https://gitlab.com/yellowhat-labs/solax/commit/7092c2bae56588a7ae8bbfdfbb4d832b6402910e))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.9 ([(b11be77)](https://gitlab.com/yellowhat-labs/solax/commit/b11be77b6f479493ba3b92af485daa65e92125b0))
* **deps:** update module github.com/prometheus/common to v0.48.0 ([(956f297)](https://gitlab.com/yellowhat-labs/solax/commit/956f2978826dd2ee8d35b8048e8169f97c6cec05))

## [1.0.8](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.7...v1.0.8) (2024-2-17)


### Bug Fixes

* **deps:** update module github.com/prometheus/client_model to v0.6.0 ([(f480503)](https://gitlab.com/yellowhat-labs/solax/commit/f480503f8c0c677ad40a39d8b69348be37101455))


### Chore

* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.7 ([(8d4ea24)](https://gitlab.com/yellowhat-labs/solax/commit/8d4ea240e856977446d9a59e36cb3151f4fe11a5))

## [1.0.7](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.6...v1.0.7) (2024-2-16)


### Bug Fixes

* **deps:** update module github.com/prometheus/common to v0.47.0 ([(201af67)](https://gitlab.com/yellowhat-labs/solax/commit/201af672ccb0f416db4d2b18f97117aee51f7c8d))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.14 ([(819103a)](https://gitlab.com/yellowhat-labs/solax/commit/819103a6e3f25a8e7994e1d090ec8cce0c2a410c))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.2 ([(6626b09)](https://gitlab.com/yellowhat-labs/solax/commit/6626b09e456418b373b47bdd3032bf7fc3c4ed7a))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.4 ([(60b0fb2)](https://gitlab.com/yellowhat-labs/solax/commit/60b0fb207153b9c901fecef57569d7d27b987221))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.5 ([(6675b5f)](https://gitlab.com/yellowhat-labs/solax/commit/6675b5f891f7b5ef643ed10ea48fa3b4e02f7f8a))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.9.6 ([(6fc2aae)](https://gitlab.com/yellowhat-labs/solax/commit/6fc2aae531d5e304e3e55a3b9ef2a3153afb9765))
* **deps:** update docker.io/golang:1.22.0 docker digest to 00ae06c ([(a42065d)](https://gitlab.com/yellowhat-labs/solax/commit/a42065d6560884e3d76441ebdabbe8b45fed1369))
* **deps:** update docker.io/golang:1.22.0 docker digest to 7b297d9 ([(1ef68d6)](https://gitlab.com/yellowhat-labs/solax/commit/1ef68d6fb10df8b873011bce2cb6412e3e68642b))
* **deps:** update docker.io/golang:1.22.0 docker digest to cefea7f ([(818ed5c)](https://gitlab.com/yellowhat-labs/solax/commit/818ed5cf56b789bc1dc1c32acadd3d1ef56e6bd6))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.56.0 ([(ca529ee)](https://gitlab.com/yellowhat-labs/solax/commit/ca529eebcece99d9629efa2c27533c0c71f2e77f))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.56.1 ([(beee4a2)](https://gitlab.com/yellowhat-labs/solax/commit/beee4a280456e63d9cf78331d6d7d49eb442192c))
* **deps:** update docker.io/golangci/golangci-lint docker tag to v1.56.2 ([(9fc11fe)](https://gitlab.com/yellowhat-labs/solax/commit/9fc11fec97b932fb1e301896f7c35e194cd076f4))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.1 ([(f85d116)](https://gitlab.com/yellowhat-labs/solax/commit/f85d116adf94391775c90ed81a16ad83e5c4a2a8))
* **deps:** update module golang.org/x/sys to v0.17.0 ([(6b8e6b2)](https://gitlab.com/yellowhat-labs/solax/commit/6b8e6b280e7cde3913c5c728cda9c39188cc5b37))

## [1.0.6](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.5...v1.0.6) (2024-2-7)


### Bug Fixes

* **deps:** update docker.io/golang docker tag to v1.22.0 ([(0255890)](https://gitlab.com/yellowhat-labs/solax/commit/0255890522d648e04ac04190a47074dc3792aca4))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.12 ([(3f437cf)](https://gitlab.com/yellowhat-labs/solax/commit/3f437cfbb91527591161b2c09a16a35901e65eff))
* **deps:** update dependency yellowhat-labs/utils to v1.4.13 ([(3da730a)](https://gitlab.com/yellowhat-labs/solax/commit/3da730abf290173705259415d8ae31061711ed88))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.5 ([(95e945c)](https://gitlab.com/yellowhat-labs/solax/commit/95e945cad3f32dbb9b6fc71228e6fbdd5ec41d06))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.6 ([(457f455)](https://gitlab.com/yellowhat-labs/solax/commit/457f455634ab1956001cf3cdfe110f1e885d403d))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.7 ([(7eea886)](https://gitlab.com/yellowhat-labs/solax/commit/7eea8862fe40fa5e497f2fb15e1b3431d16cb4bf))
* **deps:** update docker.io/golang:1.21.6 docker digest to 4d1942c ([(4a8f8a4)](https://gitlab.com/yellowhat-labs/solax/commit/4a8f8a49ab7a612c7edae5b5c50d83f1ea300656))
* **deps:** update docker.io/golang:1.21.6 docker digest to 7b575fe ([(88b2248)](https://gitlab.com/yellowhat-labs/solax/commit/88b2248e82e34abe36fb3a2f614417e5a5a8dfb7))
* **deps:** update gcr.io/kaniko-project/executor docker tag to v1.20.0 ([(1c028f2)](https://gitlab.com/yellowhat-labs/solax/commit/1c028f2deba3667ea105b671cc4b7984604985ac))

## [1.0.5](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.4...v1.0.5) (2024-1-25)


### Bug Fixes

* no need for go on metrics.Update ([(f0baefe)](https://gitlab.com/yellowhat-labs/solax/commit/f0baefeaac7963cb5a9bd48e8a8385899558bcb0))


### Chore

* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.3 ([(f59c5d4)](https://gitlab.com/yellowhat-labs/solax/commit/f59c5d4294d9907fdfa7e91e72e898b24eaf8b6e))
* **deps:** update docker.io/golang:1.21.6 docker digest to 76aadd9 ([(f81ee9a)](https://gitlab.com/yellowhat-labs/solax/commit/f81ee9a89f65fe06450057c0f31048268763cb0d))
* **deps:** update module github.com/go-openapi/swag to v0.22.9 ([(6083711)](https://gitlab.com/yellowhat-labs/solax/commit/60837112eadd9a0a9e58fefd347792d0f4540e2a))

## [1.0.4](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.3...v1.0.4) (2024-1-23)


### Bug Fixes

* **deps:** update module github.com/getkin/kin-openapi to v0.123.0 ([(22b770d)](https://gitlab.com/yellowhat-labs/solax/commit/22b770d8e02a24205301caa0b4e40b97e1e7e884))


### Chore

* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.0 ([(77683fd)](https://gitlab.com/yellowhat-labs/solax/commit/77683fdef10e7e12ec3840b53a37fde98e83b11a))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.1 ([(c3c9a2e)](https://gitlab.com/yellowhat-labs/solax/commit/c3c9a2ea31608012878328fbe6eb063ce6208f6e))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.8.2 ([(bc035dd)](https://gitlab.com/yellowhat-labs/solax/commit/bc035dd64896e5235ee0110fa20d5a7336e3acf0))
* **deps:** update docker.io/golang:1.21.6 docker digest to 5f5d61d ([(c231418)](https://gitlab.com/yellowhat-labs/solax/commit/c23141885e81ef9f349f66a6ba513fc5639a41c2))


### Continuous Integration

* fix renovate ([(2e27e8b)](https://gitlab.com/yellowhat-labs/solax/commit/2e27e8b76df21d0565ede22adaff62b51c044895))

## [1.0.3](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.2...v1.0.3) (2024-1-14)


### Bug Fixes

* **deps:** update module github.com/prometheus/common to v0.46.0 ([(127511e)](https://gitlab.com/yellowhat-labs/solax/commit/127511e84def01a8db5bc9b1765f24893662c71e))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.10 ([(4fa35de)](https://gitlab.com/yellowhat-labs/solax/commit/4fa35de9c43fcf14a8e6e2a6b184ed8e046cb9df))
* **deps:** update dependency yellowhat-labs/utils to v1.4.11 ([(dc89967)](https://gitlab.com/yellowhat-labs/solax/commit/dc899675a081d31bebeb90ac24b6d24988474a0b))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.7.1 ([(079a5c2)](https://gitlab.com/yellowhat-labs/solax/commit/079a5c2c79a5d414e37de25d2a7cb13816cf8707))
* **deps:** update docker.io/dshanley/vacuum docker tag to v0.7.2 ([(1d64156)](https://gitlab.com/yellowhat-labs/solax/commit/1d641565358739c18e76e1d94998ce68204d337b))
* **deps:** update docker.io/golang:1.21.6 docker digest to 21260a4 ([(32f271b)](https://gitlab.com/yellowhat-labs/solax/commit/32f271bcf0fb0e0907730294da2ee1dcc8f67b32))
* **deps:** update docker.io/golang:1.21.6 docker digest to 6fbd2d3 ([(ee1ef40)](https://gitlab.com/yellowhat-labs/solax/commit/ee1ef4062d8bc465f4c803d8e775be48f0af3b66))

## [1.0.2](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.1...v1.0.2) (2024-1-10)


* Merge branch 'renovate/docker.io-golang-1.x' into 'main' ([(efc4188)](https://gitlab.com/yellowhat-labs/solax/commit/efc4188b013e114dd6a37cc16085356b7d817b6e))
* Merge branch 'renovate/yellowhat-labs-utils-1.x' into 'main' ([(1393dd9)](https://gitlab.com/yellowhat-labs/solax/commit/1393dd96e59e4ddbb1592f4d19ab92639970a466))
* Merge branch 'renovate/go-openapi' into 'main' ([(c664d2e)](https://gitlab.com/yellowhat-labs/solax/commit/c664d2e42f1c39b2c15474cde25ba12336f4d270))
* Merge branch 'renovate/github.com-gorilla-mux-1.x' into 'main' ([(da59a82)](https://gitlab.com/yellowhat-labs/solax/commit/da59a82abb7a0bf1d63751e1625c8642802c8756))
* Merge branch 'openapi' into 'main' ([(d1597b6)](https://gitlab.com/yellowhat-labs/solax/commit/d1597b6d1fe35add7b91a727500f494a1bd2d94e))
* Merge branch 'renovate/yellowhat-labs-utils-1.x' into 'main' ([(2b6cefb)](https://gitlab.com/yellowhat-labs/solax/commit/2b6cefb17ee1a04c8b7e8adc4f41905c4b560f61))


### Bug Fixes

* **deps:** update docker.io/golang docker tag to v1.21.6 ([(87773ae)](https://gitlab.com/yellowhat-labs/solax/commit/87773ae30c4590aeb619b95389de4d992030fc9d))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.8 ([(7d7a9ed)](https://gitlab.com/yellowhat-labs/solax/commit/7d7a9ed1f21b0fcfaec7fdbbbb9bdd56b64a5ff2))
* **deps:** update dependency yellowhat-labs/utils to v1.4.9 ([(bfc01b9)](https://gitlab.com/yellowhat-labs/solax/commit/bfc01b98bd08f3a9a5265aad7ce9701cf26a9afe))
* **deps:** update go-openapi packages ([(1d9c3bd)](https://gitlab.com/yellowhat-labs/solax/commit/1d9c3bd94800b8304ae50dc86c34267fdad07ea8))
* **deps:** update module github.com/gorilla/mux to v1.8.1 ([(dbe3ede)](https://gitlab.com/yellowhat-labs/solax/commit/dbe3edef0f0da695946577e03c33f10ac9b79129))


### Continuous Integration

* **data:** refactor test ([(3d7cca7)](https://gitlab.com/yellowhat-labs/solax/commit/3d7cca7d941253bc964fa5f0f605dbb34a7cb89b))
* fix renovate ([(d9bbe3f)](https://gitlab.com/yellowhat-labs/solax/commit/d9bbe3f5c9e0c9b3a5b67f9f4a4cc775e8467da2))
* use openapi spec ([(f06fd66)](https://gitlab.com/yellowhat-labs/solax/commit/f06fd665e1370fdc6bd369a62bebb4699f163d37))

## [1.0.1](https://gitlab.com/yellowhat-labs/solax/compare/v1.0.0...v1.0.1) (2024-1-5)


### Bug Fixes

* update go deps ([(004129e)](https://gitlab.com/yellowhat-labs/solax/commit/004129e1d8889c9b1463eb19e9eb93aa641978c3))


* Merge branch 'renovate/docker.io-golang-1.21.5' into 'main' ([(fc7fdf4)](https://gitlab.com/yellowhat-labs/solax/commit/fc7fdf45cf08353aa3f5f5fc7fcd34f20e3518b5))
* Merge branch 'renovate/docker.io-golang-1.21.5' into 'main' ([(a706c10)](https://gitlab.com/yellowhat-labs/solax/commit/a706c101c942eecceda352bef2421fca847912a3))
* Merge branch 'renovate/yellowhat-labs-utils-1.x' into 'main' ([(219155a)](https://gitlab.com/yellowhat-labs/solax/commit/219155aa65ecc222cc86c3f62b221529b1978790))


### Chore

* **deps:** update dependency yellowhat-labs/utils to v1.4.6 ([(bfe45d9)](https://gitlab.com/yellowhat-labs/solax/commit/bfe45d9cc6111921ce2c88bcafc531e1fdeaae07))
* **deps:** update docker.io/golang:1.21.5 docker digest to 2ff79bc ([(5f30270)](https://gitlab.com/yellowhat-labs/solax/commit/5f30270bc6acbc3706b705b74b933b8de33d37ab))
* **deps:** update docker.io/golang:1.21.5 docker digest to 672a228 ([(adcf6c9)](https://gitlab.com/yellowhat-labs/solax/commit/adcf6c9dff9227e70f1ea04f6df1181410e96ce8))
* utils v1.4.5 ([(6fcdad4)](https://gitlab.com/yellowhat-labs/solax/commit/6fcdad4c5d7abf92047098f1d1b8888c41399559))

# [1.0.0](https://gitlab.com/yellowhat-labs/solax/compare/...v1.0.0) (2023-12-9)


### Bug Fixes

* initial commit ([(394150d)](https://gitlab.com/yellowhat-labs/solax/commit/394150d00c88a4de3242852695a0b5fa5c521845))
